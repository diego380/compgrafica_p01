from biblioteca.imageai.Detection import VideoObjectDetection
import os

nameVideo = input('Entre com o nome da video: ')
videoInput = 'videos/base/'+nameVideo+'.mp4'
videoOutput = 'videos/identificacao_ok/'+nameVideo
print('O video escolhido é ',videoInput)

execution_path = os.getcwd()

detector = VideoObjectDetection()
detector.setModelTypeAsRetinaNet()
detector.setModelPath( 
    os.path.join(
        execution_path ,
        "biblioteca/resnet50_coco_best_v2.0.1.h5"
    )
)

detector.loadModel(detection_speed="fast")

video_path = detector.detectObjectsFromVideo(
    input_file_path=os.path.join(execution_path, videoInput),
    output_file_path=os.path.join(execution_path, videoOutput),
    frames_per_second=20, log_progress=True
)
print(video_path)
