To use ImageAI in your application developments, you must have installed the following dependencies before you install ImageAI :


- Python 3.5.1 (and later versions) Download (Support for Python 2.7 coming soon) 
- pip3 Install 
- Tensorflow 1.4.0 (and later versions) Install or install via pip

 pip3 install --upgrade tensorflow 
- Numpy 1.13.1 (and later versions) Install or install via pip
 pip3 install numpy 
- SciPy 0.19.1 (and later versions) Install or install via pip
 pip3 install scipy 
- OpenCV Install or install via pip
 pip3 install opencv-python 
- Pillow Install or install via pip
 pip3 install pillow 
- Matplotlib Install or install via pip
 pip3 install matplotlib 
- h5py Install or install via pip
 pip3 install h5py 
- Keras 2.x Install or install via pip
 pip3 install keras 
Installation
To install ImageAI, run the python installation instruction below in the command line: 

pip3 install https://github.com/OlafenwaMoses/ImageAI/releases/download/2.0.1/imageai-2.0.1-py3-none-any.whl 