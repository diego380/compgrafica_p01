from biblioteca.imageai.Detection import ObjectDetection
from PIL import Image
import os

nameImage = input('Entre com o nome da imagem: ')
imageInput = 'imagens/base/'+nameImage+'.jpg'
imageOutout = 'imagens/identificacao_ok/'+nameImage+'.jpg'
print('A imagem escolhida é ',imageInput)

execution_path = os.getcwd()

detector = ObjectDetection()
detector.setModelTypeAsRetinaNet()
detector.setModelPath(os.path.join(
    execution_path, "biblioteca/resnet50_coco_best_v2.0.1.h5")
)
detector.loadModel()

custom_objects = detector.CustomObjects(
    car=True,
    motorcycle=True,
    person=True,
    bicycle=True,
    bus=True,
    train=True,
    truck=True,
    stop_sign=True,
    backpack=True
)

detections = detector.detectObjectsFromImage(
    input_image=os.path.join(execution_path, imageInput),
    output_image_path=os.path.join(execution_path, imageOutout)
)

cars = []
motorcycles = []
persons = []
bicycles = []
buses = []
trains=[]
trucks=[]
stop_signs=[]
backpacks=[]
for eachObject in detections:
    if (eachObject["name"]=="car"):
        car = eachObject["name"]
        cars.append(car)
    elif (eachObject["name"]=="motorcycle"):
        motorcycle = eachObject["name"]
        motorcycles.append(motorcycle)
    elif (eachObject["name"]=="person"):
        person = eachObject["name"]
        persons.append(person)
    elif (eachObject["name"]=="bicycle"):
        bicycle = eachObject["name"]
        bicycles.append(bicycle)
    elif (eachObject["name"]=="train"):
        train = eachObject["name"]
        trains.append(train)
    elif (eachObject["name"]=="truck"):
        truck = eachObject["name"]
        trucks.append(truck)
    elif (eachObject["name"]=="stop_sign"):
        stop_sign = eachObject["name"]
        stop_signs.append(stop_sign)
    elif (eachObject["name"]=="backpack"):
        backpack = eachObject["name"]
        backpacks.append(backpack)


print("O total de carros é: ", len(cars))
print("O total de motos é: ", len(motorcycles))
print("O total de pessoas é: ", len(persons))
print("O total de bicicletas é: ", len(bicycles))
print("O total de onibus é: ", len(buses))
print("O total de trens é: ", len(trains))
print("O total de caminhões é: ", len(trucks))
print("O total de placas de pare é: ", len(stop_signs))
print("O total de bolsas de costas é: ", len(backpacks))
print("Total de objetos é ", len(detections))

Image.open(imageInput).show()
Image.open(imageOutout).show()